This is a retail website for a candy business I'm starting. MVP includes being able to access the site and get some basic information about the product and how to order. V2 will include financial transaction functionality. V3 will include pictures and more snazzy marketing text.

Database: Postgresql

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact